package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


interface Customer {
    void update(Object o);
}

class NewsAgency {
    private String news;
    private List<Customer> customers = new ArrayList<>();

    public void addObserver(Customer channel) {
        this.customers.add(channel);
    }

    public void removeObserver(Customer channel) {
        this.customers.remove(channel);
    }

    public void setNews(String news) {
        this.news = news;
        for (Customer customer : this.customers) {
            customer.update(this.news);
        }
    }
}

class NewCustomer implements Customer {
    private String news;

    @Override
    public void update(Object news) {
        this.setNews((String) news);
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }
}

public class Main  {
    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        NewsAgency observable = new NewsAgency();
        NewCustomer observer = new NewCustomer();

        observable.addObserver(observer);
        //new agency send info about new news
        observable.setNews("news");
        //customer get the info
        observer.getNews();

        if (observer.getNews().equals("news")) {
            logger.info("Customer get his news and is happy");
        }
    }


}
